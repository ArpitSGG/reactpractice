export const footerData = [
    {
        heading: 'Company',
        items: ['Team', 'History', 'Contact Us', 'Locations'],
    },
    {
        heading: 'Features',
        items: [
            'Cool stuff',
            'Random feature',
            'Team feature',
            'Developer stuff',
            'Another one',
        ],
    },
    {
        heading: 'Resources',
        items: [
            'Resource',
            'Resource Name',
            'Another resource',
            'Final resource',
        ],
    },
    {
        heading: 'Legal',
        items: ['Privacy policy', 'Terms of use'],
    },
]
