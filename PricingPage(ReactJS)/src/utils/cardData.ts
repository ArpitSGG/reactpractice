export const cardData = [
    {
        plan: 'Free',
        popular: false,
        price: 0,
        features: [
            '10 users included',
            '2 GB of storage',
            'Help center access',
            'Email support',
        ],
        button: 'SIGN UP FOR FREE',
    },
    {
        plan: 'Pro',
        popular: true,
        price: 15,
        features: [
            '20 users included',
            '10 GB of storage',
            'Help center access',
            'Priority email support',
        ],
        button: 'GET STARTED',
    },
    {
        plan: 'Enterprise',
        popular: false,
        price: 30,
        features: [
            '50 users included',
            '30 GB of storage',
            'Help center access',
            'Phone & email support',
        ],
        button: 'CONTACT US',
    },
]
