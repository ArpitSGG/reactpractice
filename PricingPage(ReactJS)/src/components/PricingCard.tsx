import classes from './PricingCard.module.css'
import { Star } from 'react-feather'

interface PricingCardProps {
    plan: string
    popular?: boolean
    price: number
    features: string[]
    button: string
}

const PricingCard = ({
    plan,
    popular = false,
    price,
    features,
    button,
}: PricingCardProps) => {
    return (
        <div className={classes.card}>
            <div className={classes.cardHeader}>
                <h3 className={classes.heading}>{plan}</h3>
                {popular && (
                    <span className={classes.optionalDescription}>
                        Most Popular
                    </span>
                )}
            </div>

            <div className={classes.cardContent}>
                <div className={classes.price}>
                    <span className={classes.amount}>${price}</span>/mo
                </div>
                <ul className={classes.featureList}>
                    {features.map((feature) => (
                        <li>{feature}</li>
                    ))}
                </ul>
                <button
                    type='submit'
                    className={`${classes.btn} ${
                        popular ? classes.primary : classes.secondary
                    }`}
                >
                    {button}
                </button>
                {popular && <Star className={classes.star} />}
            </div>
        </div>
    )
}

export default PricingCard
