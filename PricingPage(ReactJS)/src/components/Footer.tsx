import FooterItem from './FooterItem'
import classes from './Footer.module.css'
import { footerData } from '../utils/footerData'

const Footer = () => {
    return (
        <footer className={classes.footer}>
            <div className={`flex justifyCenter ${classes.footerItemWrapper}`}>
                {footerData.map((item) => (
                    <FooterItem heading={item.heading} items={item.items} />
                ))}
            </div>
        </footer>
    )
}

export default Footer
