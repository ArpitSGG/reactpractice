import classes from './Footer.module.css'

interface FooterProps {
    heading: string
    items: string[]
}

const FooterItem = ({ heading, items }: FooterProps) => {
    return (
        <div className={classes.footerItem}>
            <h4 className={classes.heading}>{heading}</h4>
            <ul className={classes.itemList}>
                {items.map((item) => (
                    <li>
                        <a className={classes.links} href='#'>
                            {item}
                        </a>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default FooterItem
