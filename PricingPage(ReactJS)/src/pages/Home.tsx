import PricingCard from '../components/PricingCard'
import classes from './Home.module.css'
import defaults from '../default.module.css'
import Footer from '../components/Footer'
import { cardData } from '../utils/cardData'

const Home = () => {
    return (
        <div className={`container flexCol alignCenter`}>
            <h1 className={classes.heading}>Pricing</h1>
            <p className={classes.description}>
                Quickly build an effective pricing table for your potential
                customers with this layout. It's built with default Material-UI
                components with little customization.
            </p>

            <section className={`flex ${classes.cardContainer}`}>
                {cardData.map((card) => (
                    <PricingCard
                        plan={card.plan}
                        popular={card.popular}
                        price={card.price}
                        features={card.features}
                        button={card.button}
                    />
                ))}
            </section>

            <hr className={classes.hr} />

            <Footer />
        </div>
    )
}

export default Home
